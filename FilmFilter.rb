require 'rubygems'
require 'open-uri'
require 'nokogiri'
require 'restclient'
require 'optparse'

IMDB_URL = 'http://www.imdb.com'
FA_URL = 'http://www.filmaffinity.com'
PYK_URL = 'http://www.peliculasyonkis.com'

OPT_MSG = 'Optional input file. input.txt by default'
OUT_MSG = 'Optional output file. output.txt by default'
AVR_MSG = 'Optional minimal average to show files. 6.0 by default'

# Printing method
def print_results(head, films, file)
  file.write(head)
  puts head
  films.each do |film|
    output_line = "#{film[:title]}\n"\
    "IMDB: #{film[:IMDB_rate]}	"\
    "FA: #{film[:FA_rate]}		"\
    "YK: #{film[:PYK_rate].round(1)}		"\
    "AVERAGE: #{film[:avg].round(2)} \n\n"
    file.write(output_line)
    puts output_line
  end
end

# ARGV options
options = {}
OptionParser.new do |opts|
  options[:ipt] = false
  opts.on('-i', '--optional [INPUT]', OPT_MSG) do |ipt|
    options[:input] = ipt || 'input.txt'
  end
  opts.on('-o', '--output [OUTPUT]', OUT_MSG) do |opt|
    options[:output] = opt || 'output.txt'
  end
  opts.on('-a', '--float [AVERAGE]', Float, AVR_MSG) do |avg|
    options[:avg] = avg || 6.0
  end
end.parse!

# No ARGV default options
options[:input] ||= 'input.txt'
options[:output] ||= 'output.txt'
options[:avg] ||= 6.0

# Reading input
films = []
File.open(options[:input], 'r').each_line do |line|
  film = {}
  film[:title] = line.delete("\n").tr('ñ', 'n')
  films << film
end

# Crawling process
films.each do |film|
  film_get = film[:title].tr(' ', '+')

  # IMDB
  html_object = Nokogiri::HTML(open(IMDB_URL + "/find?q=#{film_get}&s=all")).css('td.result_text a').first
  if html_object
    film_href = html_object.attributes['href']
    html_object = Nokogiri::HTML(open(IMDB_URL + film_href))
    if html_object.css('div.findNoResults').text == ''
      film[:IMDB_rate] = html_object.css('div.star-box-giga-star').first.text.to_f
    end
  end

  # Filmaffinity
  html_object = Nokogiri::HTML(open(FA_URL + "/es/search.php?stext=#{film_get}&stype=all"))

  # Search page with results
  if html_object.css('h1').first.text == 'Resultados por título'
    film_href = html_object.css('div.mc-title > a').first.attributes['href']
    html_object = Nokogiri::HTML(open(FA_URL + film_href))
    film[:FA_rate] = html_object.css('div#movie-rat-avg').text.to_f

  # Redirection (no search page without results)
  elsif html_object.css('div.gs-no-results-result').text == ''
    film[:FA_rate] = html_object.css('div#movie-rat-avg').text.to_f
  end

  # PeliculasYonkis
  data = { 'keyword' => "#{film[:title]}", 'search_type' => 'pelicula' }
  html_object = Nokogiri::HTML(RestClient.post(PYK_URL + '/buscar/pelicula', data))
  if html_object.css('span#total_peliculas').text.to_i > 0
    film[:PYK_rate] = html_object.css('li.nth-child1n > figure > figcaption').text.to_f
  end

  # Average calculation
  filtered_rates = [film[:IMDB_rate], film[:FA_rate], film[:PYK_rate]].compact
  film[:avg] = filtered_rates.size > 0 ? filtered_rates.reduce(0, :+) / filtered_rates.size : 0
end

# Writing and printing
out = File.open(options[:output], 'w')
films = films.partition { |film| film[:avg] >= options[:avg] }

head = "----------------------- VALID FILMS --------------------------\n"
print_results(head, films[0], out)

head = "\n\n--------------------- INVALID FILMS --------------------------\n"
print_results(head, films[1], out)

out.close
