# FilmFilter Readme

Given a list of films, search their ratings in different websites (IMDB, Filmaffinity, PeliculasYonkis) and filter them by a given average.
It only calculates the average using the best result of the websites with results 


---- USAGE: ----

ruby FilmFilter.rb -a [targetAverage] - i [inputFileName] -o [outputFileName]

-  -a targetAverage: Only films with higher rates than targetAverage will be written in the output file
    DEFAULT: 6
  
-  -i inputFileName input file name
    DEFAULT: 'list.txt'

-  -o output file name
    DEFAULT: 'output.txt'

  i.e: ruby FilmFilter.rb -a 6 -i 'list.txt' -o 'output.txt' 

All arguments are optional

---- INPUT FILE: ----

File with the films to search (1 film in each line). An input file example is  given.
'list.txt' by default


---- OUTPUT FILE: ----

File with the filtered films. An output file example is given.
'output.txt' by default